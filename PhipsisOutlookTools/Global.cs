﻿using PhipsisOutlookTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools
{
    public static class Global
    {

        public static string UserProfilePath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "PhipsisOutlookTools"
                );

        public static FolderToEMailList folderToEMailList;

        public static DuplicateFolderList duplicateFolderList;

        public static string CurrentlySelectedFolder;
        internal static string Version;

        public delegate void CurrentlySelectedFolderHandler(string folder);

        public static event CurrentlySelectedFolderHandler CurrentlySelectedFolderChanged;

        public static void ChangeCurrentlySelectedFolder(string folder)
        {
            CurrentlySelectedFolder = folder;
            CurrentlySelectedFolderChanged?.Invoke(folder);
        }


    }
}
