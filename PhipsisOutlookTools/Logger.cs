﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools
{
    public class Logger
    {
        private bool writeDebug;
        private string className;

        public delegate void LogWriteDelegate(string line);
        public static event LogWriteDelegate LogWrite;


        public Logger(string className)
        {
            writeDebug = Debugger.IsAttached;
            this.className = className;
        }

        public void Info(string text, params string[] args)
        {
            Log("INFO", text, args);
        }


        public void Error(string text, params string[] args)
        {
            Log("ERROR", text, args);
        }

        public void Log(string category, string text, params string[] args)
        {
            string line = string.Format("{0} {1} [{2}]: {3}", 
                DateTime.Now.ToString(),
                category, 
                className, 
                string.Format(text, args));
            if (writeDebug) Debug.WriteLine(line);
            LogWrite?.Invoke(line);
        }

    }
}
