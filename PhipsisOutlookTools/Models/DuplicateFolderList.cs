﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools.Models
{
    public class DuplicateFolderList
    {

        public BindingList<DuplicateFolderEntry> Data = new BindingList<DuplicateFolderEntry>();

        public string Filepath;


        public DuplicateFolderList(string path)
        {
            Filepath = path;
        }

        public void Save()
        {

            var jsontext = JsonConvert.SerializeObject(Data);

            File.WriteAllText(Filepath, jsontext);
        }

        public void TryLoad()
        {
            if (File.Exists(Filepath))
            {
                var jsontext = File.ReadAllText(Filepath);

                Data = JsonConvert.DeserializeObject<BindingList<DuplicateFolderEntry>>(jsontext);
            }
        }

        internal DuplicateFolderEntry[] SearchByFolderFullPath(string fullpath)
        {
            var dfes = Data.Where(p => p.FolderPathToDeleteItem == fullpath
                || p.FolderPathToKeepItem == fullpath).ToArray();

            return dfes;
        }

        internal DuplicateFolderEntry Add(string name,
            string folderPathToKeepItem,
            string folderPathToDeleteItem)
        {
            long id = 1;

            var newestEntry = Data.OrderByDescending(i => i.Id).FirstOrDefault();
            if (newestEntry != null)
            {
                id = newestEntry.Id + 1;
            }

            var dfe = new DuplicateFolderEntry(id, name, folderPathToKeepItem, folderPathToDeleteItem);

            Data.Add(dfe);

            return dfe;
        }
    }
}
