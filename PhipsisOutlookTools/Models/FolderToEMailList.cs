﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools.Models
{
    public class FolderToEMailList
    {
        public BindingList<FolderToEMail> Data = new BindingList<FolderToEMail>();

        

        public string Filepath;

        private Dictionary<string, FolderToEMail> wildCardMapping = new Dictionary<string, FolderToEMail>();
        private bool recentlyChanged = true;

        public FolderToEMailList(string path)
        {
            Filepath = path;
        }


        private void UpdateCache()
        {
            if(recentlyChanged)
            {
                wildCardMapping.Clear();
                foreach(var f in Data.Where(f => f.Enabled && f.Folder.EndsWith("*")))
                {
                    wildCardMapping.Add(
                        f.Folder.Remove(f.Folder.Length - 1),
                        f);
                }
                recentlyChanged = false;
            }
        }

        public void Save()
        {

            recentlyChanged = true;

            var jsontext = JsonConvert.SerializeObject(Data);

            File.WriteAllText(Filepath, jsontext);
        }

        public void TryLoad()
        {
            if(File.Exists(Filepath))
            {
                var jsontext = File.ReadAllText(Filepath);

                Data = JsonConvert.DeserializeObject<BindingList<FolderToEMail>>(jsontext);
            }
        }

        public FolderToEMail SearchByFolder(string folder)
        {
            var foundFolderToEMail = Data.Where(p => p.Enabled && p.Folder == folder).FirstOrDefault();
            if (foundFolderToEMail != null) return foundFolderToEMail;

            UpdateCache();

            var foundFolderToEMailWildCard = wildCardMapping.Where(p => folder.StartsWith(p.Key)).FirstOrDefault();
            if (!foundFolderToEMailWildCard.Equals(default(KeyValuePair<string, FolderToEMail>)))  return foundFolderToEMailWildCard.Value;

            return null;
        }

        internal void TryAdd(FolderToEMail folderToEMail)
        {
            var id = 1;

            if (Data.Any(d => d.Folder == folderToEMail.Folder)) return;

            var newestEntry = Data.OrderByDescending(i => i.Id).FirstOrDefault();
            if(newestEntry != null)
            {
                id = newestEntry.Id + 1;
            }
            folderToEMail.Id = id;

            recentlyChanged = true;

            Data.Add(folderToEMail);
        }
    }
}
