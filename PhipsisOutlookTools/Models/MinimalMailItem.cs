﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PhipsisOutlookTools.Models
{
    public class MinimalMailItem
    {

        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public MinimalMailItem(Microsoft.Office.Interop.Outlook.MailItem mailItem)
        {
            this.Recipients = Helper.RecipientsToString(mailItem.Recipients);
            this.Subject = mailItem.Subject;
            this.Body = mailItem.Body;
        }

        public override bool Equals(object obj)
        {
            if(obj is MinimalMailItem)
            {
                var item = (MinimalMailItem)obj;
                return 
                   Recipients == item.Recipients &&
                   Subject == item.Subject &&
                   Body == item.Body;
            }
            if(obj is Microsoft.Office.Interop.Outlook.MailItem mailItem)
            {
                var item = (Microsoft.Office.Interop.Outlook.MailItem)obj;
                return 
                   Recipients == item.To &&
                   Subject == item.Subject &&
                   Body == item.Body;
            }
            return false;
        }

        public override string ToString()
        {
            var cleanBody = Body;
            if (cleanBody.Length > 20) cleanBody = cleanBody.Substring(0, 20) + "...";
            cleanBody = Regex.Replace(cleanBody, @"\t|\n|\r", "");
            return string.Format("Recipients<<{0}>>,Subject<<{1}>>,Body<<{2}>>", Recipients, Subject, cleanBody);
        }
    }
}
