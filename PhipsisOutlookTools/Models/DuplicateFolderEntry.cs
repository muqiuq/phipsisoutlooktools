﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools.Models
{
    public class DuplicateFolderEntry : INotifyPropertyChanged
    {
        public DuplicateFolderEntry(long id, 
            string name, 
            string folderPathToKeepItem, 
            string folderPathToDeleteItem)
        {
            Id = id;
            Name = name;
            FolderPathToKeepItem = folderPathToKeepItem;
            FolderPathToDeleteItem = folderPathToDeleteItem;
        }

        public long Id { get; set; }

        public bool Enabled { get; set; }
        
        public string Name { get; set; }

        public string FolderPathToKeepItem { get; set; }

        public string FolderPathToDeleteItem { get; set; }

        public void TriggerChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Folder"));
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}: {2}",
                Enabled ? "x" : " ",
                Id,
                Name
               );
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
