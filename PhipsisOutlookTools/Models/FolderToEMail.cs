﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools.Models
{
    public class FolderToEMail : INotifyPropertyChanged
    {
        public FolderToEMail(bool enabled, string folder, string email)
        {
            Enabled = enabled;
            Folder = folder;
            Email = email;
        }

        public bool Enabled { get; set; }

        public int Id { get; set; }

        public string Folder { get; set; }

        public string Email { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void TriggerChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Folder"));
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}: {2} => {3}",
                Enabled ? "x" : " ",
                Id,
                Folder,
                Email
                );
        }
    }
}
