﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using PhipsisOutlookTools.Forms.DebugForms;
using PhipsisOutlookTools.Forms.DuplicateFolderForms;
using PhipsisOutlookTools.Forms.FolderToEmailForms;

namespace PhipsisOutlookTools
{
    public partial class BasicToolsRibbon
    {
        private void BasicToolsRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            labelVersion.Label = "Version: " + PhipsisOutlookTools.Global.Version;
        }

        private void calendarTestButton_Click(object sender, RibbonControlEventArgs e)
        {
            var currentFolder = Globals.ThisAddIn.Application.ActiveExplorer().CurrentFolder;

            Microsoft.Office.Interop.Outlook.Items items = currentFolder.Items;

            string toRemove = "Teilnahme erforderlich";

            foreach (var item in items)
            {
                if(!(item is Microsoft.Office.Interop.Outlook.AppointmentItem))
                {
                    MessageBox.Show("Are you in the calendar view?");
                    return;
                }
                
                var calendarItem = (Microsoft.Office.Interop.Outlook.AppointmentItem) item;

                if (calendarItem.Categories.Contains(toRemove))
                {
                    Debug.WriteLine($"{calendarItem.Subject} ({calendarItem.Start}) - {calendarItem.Categories}");

                    var catItems = calendarItem.Categories.Split(';');

                    var newCatItems = new List<string>();

                    foreach(var catItem in catItems)
                    {
                        if(!catItem.Trim().Equals(toRemove))
                        {
                            newCatItems.Add(catItem);
                        }
                    }

                    var newCat = string.Join(";", newCatItems.ToArray());

                    Debug.WriteLine("Categorie neu: " + newCat);

                    calendarItem.Categories = newCat;

                    calendarItem.Save();

                    break;
                }
                
                
            }

            
        }

        private void batchEditAppCatButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new BatchEditAppointmentCategoriesForm();

            form.Show();
        }

        private void categoryAsTextEditorButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new CategoriesAsTextEditorForm();

            form.Show();
        }

        private void buttonFolderToEmail_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new FolderToEmailListForm();

            form.Show();
        }

        private void buttonDuplicateFolder_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new DuplicateFolderForm();

            form.Show();
        }

        private void buttonDebug_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new DebugForm();

            form.Show();
        }

        private void buttonWebsite_Click(object sender, RibbonControlEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/muqiuq/phipsisoutlooktools/");
        }
    }
}
