﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhipsisOutlookTools
{
    public partial class CategoriesAsTextEditorForm : Form
    {
        private Microsoft.Office.Interop.Outlook.Application Application;

        class SimpleCategoryEntry
        {
            public OlCategoryColor Color;
            public string Name;
            public string CategoryId;
        }

        public CategoriesAsTextEditorForm()
        {
            InitializeComponent();

            this.Application = Globals.ThisAddIn.Application;

            foreach (var color in Enum.GetNames(typeof(OlCategoryColor)))
            {
                colorNamesRichTextBox.Text += color + "\n";
            }

            loadButton_Click(null, null);
        }

        private List<SimpleCategoryEntry> ParseCategoriesRichTextBox(out int numOfLines)
        {
            var inputText = catRichTextBox.Text;

            var inputTextSpl = inputText.Split('\n');

            var entries = new List<SimpleCategoryEntry>();

            numOfLines = inputTextSpl.Length;

            foreach (var line in inputTextSpl)
            {
                var elements = line.Split(',');
                var pCategoryId = "";
                var pName = "";
                var pColor = "";
                if (elements.Length == 3)
                {
                    pCategoryId = elements[0];
                    pName = elements[2];
                    pColor = elements[1];
                }
                else if (elements.Length == 2)
                {
                    pName = elements[1];
                    pColor = elements[0];
                }
                if (pName.Trim() == "") continue;
                if (Enum.TryParse<OlCategoryColor>(pColor.Trim(), out var colorValue))
                {
                    if (Enum.IsDefined(typeof(OlCategoryColor), colorValue))
                    {
                        entries.Add(new SimpleCategoryEntry() { CategoryId = pCategoryId.Trim(), Color = colorValue, Name = pName.Trim() });
                    }
                }
            }

            return entries;
        }

        private void validateButton_Click(object sender, EventArgs e)
        {
            var entries = ParseCategoriesRichTextBox(out var numOfLines);

            statusLabel.Text = $"Valid: {entries.Count} / {numOfLines}";
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            catRichTextBox.Text = "";
            foreach (Category category in Application.Session.Categories)
            {
                catRichTextBox.Text += ($"{category.CategoryID}, {category.Color}, {category.Name}\n");
            }
            if(catRichTextBox.Text.Length > 0)
            {
                catRichTextBox.Text = catRichTextBox.Text.Substring(0, catRichTextBox.Text.Length - 1);
            }
        }

        private void button1_Click(object sender, EventArgs ea)
        {
            var categories = Application.Session.Categories;

            var entries = ParseCategoriesRichTextBox(out var numOfLines);

            int updated = 0;
            int added = 0;
            int deleted = 0;

            foreach (var entry in entries)
            {
                if (categories[entry.Name] != null)
                {
                    categories[entry.Name].Color = entry.Color;
                    updated++;
                }
                else if (entry.CategoryId != "" && categories[entry.CategoryId] != null)
                {
                    categories[entry.CategoryId].Color = entry.Color;
                    categories[entry.CategoryId].Name = entry.Name;
                    updated++;
                }
                else
                {
                    categories.Add(entry.Name, entry.Color);
                    added++;
                }
            }

            var originalCategories = new List<object>();
            foreach (var c in categories)
            {
                originalCategories.Add(c);
            }

            foreach (var oc in originalCategories)
            {
                if (!entries.Any(e => e.Name == ((Category)oc).Name))
                {
                    categories.Remove(((Category)oc).Name);
                    deleted++;
                }
            }

            statusLabel.Text = $"Added: {added}, Updated: {updated}, Deleted: {deleted}";
        }

        private void RemoveIdsButton_Click(object sender, EventArgs e)
        {
            catRichTextBox.Text = "";
            foreach (Category category in Application.Session.Categories)
            {
                catRichTextBox.Text += ($"{category.Color}, {category.Name}\n");
            }
        }

        private void colorNamesRichTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = colorNamesRichTextBox.SelectionStart;
            int line = colorNamesRichTextBox.GetLineFromCharIndex(index);
            catRichTextBox.Text += "\n" + colorNamesRichTextBox.Lines[line] + ",";
        }
    }
}
