﻿namespace PhipsisOutlookTools
{
    partial class CategoriesAsTextEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.catRichTextBox = new System.Windows.Forms.RichTextBox();
            this.validateButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.colorNamesRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.loadButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.loadNoIdsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // catRichTextBox
            // 
            this.catRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.catRichTextBox.DetectUrls = false;
            this.catRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catRichTextBox.Location = new System.Drawing.Point(12, 42);
            this.catRichTextBox.Name = "catRichTextBox";
            this.catRichTextBox.Size = new System.Drawing.Size(665, 447);
            this.catRichTextBox.TabIndex = 0;
            this.catRichTextBox.Text = "";
            this.catRichTextBox.WordWrap = false;
            // 
            // validateButton
            // 
            this.validateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.validateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validateButton.Location = new System.Drawing.Point(222, 495);
            this.validateButton.Name = "validateButton";
            this.validateButton.Size = new System.Drawing.Size(169, 23);
            this.validateButton.TabIndex = 1;
            this.validateButton.Text = "Validate";
            this.validateButton.UseVisualStyleBackColor = true;
            this.validateButton.Click += new System.EventHandler(this.validateButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(683, 500);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(16, 13);
            this.statusLabel.TabIndex = 2;
            this.statusLabel.Text = "...";
            // 
            // colorNamesRichTextBox
            // 
            this.colorNamesRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.colorNamesRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colorNamesRichTextBox.Location = new System.Drawing.Point(683, 42);
            this.colorNamesRichTextBox.Name = "colorNamesRichTextBox";
            this.colorNamesRichTextBox.ReadOnly = true;
            this.colorNamesRichTextBox.Size = new System.Drawing.Size(195, 447);
            this.colorNamesRichTextBox.TabIndex = 3;
            this.colorNamesRichTextBox.Text = "";
            this.colorNamesRichTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.colorNamesRichTextBox_MouseDoubleClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(680, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Valid Color Enum Names:";
            // 
            // loadButton
            // 
            this.loadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadButton.Location = new System.Drawing.Point(12, 495);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 23);
            this.loadButton.TabIndex = 5;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(397, 495);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // loadNoIdsButton
            // 
            this.loadNoIdsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadNoIdsButton.Location = new System.Drawing.Point(93, 495);
            this.loadNoIdsButton.Name = "loadNoIdsButton";
            this.loadNoIdsButton.Size = new System.Drawing.Size(123, 23);
            this.loadNoIdsButton.TabIndex = 7;
            this.loadNoIdsButton.Text = "Load (no ids)";
            this.loadNoIdsButton.UseVisualStyleBackColor = true;
            this.loadNoIdsButton.Click += new System.EventHandler(this.RemoveIdsButton_Click);
            // 
            // CategoriesAsTextEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 530);
            this.Controls.Add(this.loadNoIdsButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.colorNamesRichTextBox);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.validateButton);
            this.Controls.Add(this.catRichTextBox);
            this.Name = "CategoriesAsTextEditorForm";
            this.Text = "Categories text editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox catRichTextBox;
        private System.Windows.Forms.Button validateButton;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.RichTextBox colorNamesRichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button loadNoIdsButton;
    }
}