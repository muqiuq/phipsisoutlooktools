﻿namespace PhipsisOutlookTools
{
    partial class BasicToolsRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public BasicToolsRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();

            RibbonType = "Microsoft.Outlook.Explorer";
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.buttonFolderToEmail = this.Factory.CreateRibbonButton();
            this.buttonDuplicateFolder = this.Factory.CreateRibbonButton();
            this.categoriesGroup = this.Factory.CreateRibbonGroup();
            this.batchEditAppCatButton = this.Factory.CreateRibbonButton();
            this.categoryAsTextEditorButton = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.buttonDebug = this.Factory.CreateRibbonButton();
            this.buttonWebsite = this.Factory.CreateRibbonButton();
            this.labelVersion = this.Factory.CreateRibbonLabel();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.categoriesGroup.SuspendLayout();
            this.group2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Groups.Add(this.categoriesGroup);
            this.tab1.Groups.Add(this.group2);
            this.tab1.Label = "PhipsisTools";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.buttonFolderToEmail);
            this.group1.Items.Add(this.buttonDuplicateFolder);
            this.group1.Label = "Configuration";
            this.group1.Name = "group1";
            // 
            // buttonFolderToEmail
            // 
            this.buttonFolderToEmail.Image = global::PhipsisOutlookTools.Properties.Resources.folder;
            this.buttonFolderToEmail.Label = "Folder to email mapping";
            this.buttonFolderToEmail.Name = "buttonFolderToEmail";
            this.buttonFolderToEmail.ShowImage = true;
            this.buttonFolderToEmail.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonFolderToEmail_Click);
            // 
            // buttonDuplicateFolder
            // 
            this.buttonDuplicateFolder.Image = global::PhipsisOutlookTools.Properties.Resources.duplicate;
            this.buttonDuplicateFolder.Label = "Duplicate Item Folder Manager";
            this.buttonDuplicateFolder.Name = "buttonDuplicateFolder";
            this.buttonDuplicateFolder.ShowImage = true;
            this.buttonDuplicateFolder.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDuplicateFolder_Click);
            // 
            // categoriesGroup
            // 
            this.categoriesGroup.Items.Add(this.batchEditAppCatButton);
            this.categoriesGroup.Items.Add(this.categoryAsTextEditorButton);
            this.categoriesGroup.Label = "Edit categories";
            this.categoriesGroup.Name = "categoriesGroup";
            // 
            // batchEditAppCatButton
            // 
            this.batchEditAppCatButton.Enabled = false;
            this.batchEditAppCatButton.Label = "Batch edit appointment categories";
            this.batchEditAppCatButton.Name = "batchEditAppCatButton";
            this.batchEditAppCatButton.Visible = false;
            this.batchEditAppCatButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.batchEditAppCatButton_Click);
            // 
            // categoryAsTextEditorButton
            // 
            this.categoryAsTextEditorButton.Label = "Categories text editor";
            this.categoryAsTextEditorButton.Name = "categoryAsTextEditorButton";
            this.categoryAsTextEditorButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.categoryAsTextEditorButton_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.buttonDebug);
            this.group2.Items.Add(this.buttonWebsite);
            this.group2.Items.Add(this.labelVersion);
            this.group2.Label = "Info";
            this.group2.Name = "group2";
            // 
            // buttonDebug
            // 
            this.buttonDebug.Image = global::PhipsisOutlookTools.Properties.Resources.tools;
            this.buttonDebug.Label = "Debug";
            this.buttonDebug.Name = "buttonDebug";
            this.buttonDebug.ShowImage = true;
            this.buttonDebug.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDebug_Click);
            // 
            // buttonWebsite
            // 
            this.buttonWebsite.Image = global::PhipsisOutlookTools.Properties.Resources.website;
            this.buttonWebsite.Label = "About (website)";
            this.buttonWebsite.Name = "buttonWebsite";
            this.buttonWebsite.ShowImage = true;
            this.buttonWebsite.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonWebsite_Click);
            // 
            // labelVersion
            // 
            this.labelVersion.Label = "Version: ";
            this.labelVersion.Name = "labelVersion";
            // 
            // BasicToolsRibbon
            // 
            this.Name = "BasicToolsRibbon";
            this.RibbonType = "Microsoft.Outlook.Mail.Read";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.BasicToolsRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.categoriesGroup.ResumeLayout(false);
            this.categoriesGroup.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup categoriesGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton batchEditAppCatButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton categoryAsTextEditorButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonFolderToEmail;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDuplicateFolder;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDebug;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonWebsite;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel labelVersion;
    }

    partial class ThisRibbonCollection
    {
        internal BasicToolsRibbon BasicToolsRibbon
        {
            get { return this.GetRibbon<BasicToolsRibbon>(); }
        }
    }
}
