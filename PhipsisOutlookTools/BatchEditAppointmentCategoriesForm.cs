﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhipsisOutlookTools
{
    public partial class BatchEditAppointmentCategoriesForm : Form
    {
        private MAPIFolder currentFolder;

        public BatchEditAppointmentCategoriesForm()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            numOfMatchLabel.Text = "N/A";
        }

        private void setProgress(int value)
        {
            if(this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    setProgress(value);
                });
                return;
            }
            progressBar1.Value = value;
        }

        private void setProgressMax(int value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    setProgressMax(value);
                });
                return;
            }
            progressBar1.Maximum = value;
        }


        private void setStatus(string value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    setStatus(value);
                });
                return;
            }
            statusLabel.Text = value;
        }

        private void setNumOfItems(string value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    setNumOfItems(value);
                });
                return;
            }
            numOfMatchLabel.Text = value;
        }

        private void countButton_Click(object sender, EventArgs e)
        {
            doCountWith();
        }

        private void setFolderButton_Click(object sender, EventArgs e)
        {
            currentFolder = Globals.ThisAddIn.Application.ActiveExplorer().CurrentFolder;

            selectedFolderLabel.Text = currentFolder.Name;

            if(currentFolder.DefaultItemType != Microsoft.Office.Interop.Outlook.OlItemType.olAppointmentItem)
            {
                statusLabel.Text = $"Invalid Item type ({currentFolder.DefaultItemType})";
            }
            else
            {
                statusLabel.Text = $"OK";
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            doCountWith(remove: true);
        }

        private void doCountWith(bool remove = false)
        {
            string actionText = "Couting";
            if(remove)
            {
                actionText = "Removing";
            }

            var single = singleCheckBox.Checked;

            var t = new Thread(() =>
            {
                if (currentFolder == null) return;

                setStatus($"{actionText}...");

                Microsoft.Office.Interop.Outlook.Items items = currentFolder.Items;

                string toCount = categoryTextBox.Text;

                var totalItems = items.Count;

                setProgressMax(totalItems);

                int c = 0;

                int cTot = 0;

                int updateCounter = 0;

                while (true)
                {
                    var item = items.GetNext();

                    if (item == null) break;

                    if (!(item is Microsoft.Office.Interop.Outlook.AppointmentItem))
                    {
                        MessageBox.Show("Are you in the calendar view?");
                        return;
                    }

                    var calendarItem = (Microsoft.Office.Interop.Outlook.AppointmentItem)item;

                    if (calendarItem.Categories != null)
                    {
                        var catItems = calendarItem.Categories.Split(';');

                        if (catItems.Contains(toCount))
                        {
                            c++;
                            if(remove)
                            {
                                var newCatItems = new List<string>();

                                foreach (var catItem in catItems)
                                {
                                    if (!catItem.Trim().Equals(toCount))
                                    {
                                        newCatItems.Add(catItem);
                                    }
                                }

                                var newCat = string.Join(";", newCatItems.ToArray());

                                calendarItem.Categories = newCat;

                                calendarItem.Save();

                                if (single)
                                {
                                    Debug.WriteLine($"{calendarItem.Subject} ({calendarItem.Start}) = {calendarItem.Categories}");
                                    break;
                                }
                            }
                        }
                    }
                    cTot++;
                    updateCounter++;
                    if (updateCounter >= 50)
                    {
                        setProgress(cTot);
                        setStatus($"({c}) {cTot}/{totalItems}");
                        updateCounter = 0;
                    }
                }
                setNumOfItems(c.ToString());
                setStatus($"{actionText} done...");
            });

            t.Start();
        }
    }
}
