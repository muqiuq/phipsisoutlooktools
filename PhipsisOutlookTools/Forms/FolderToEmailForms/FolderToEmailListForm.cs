﻿using PhipsisOutlookTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhipsisOutlookTools.Forms.FolderToEmailForms
{
    public partial class FolderToEmailListForm : Form
    {
        private FolderToEMailList folderToEmailList;
        private FolderToEMail currentlySelectedFolderToEmail;

        public FolderToEmailListForm()
        {
            InitializeComponent();

            folderToEmailList = Global.folderToEMailList;

            Global.CurrentlySelectedFolderChanged += Global_CurrentlySelectedFolderChanged;

            textBoxFolderNew.Text = Global.CurrentlySelectedFolder;

            UpdateList();
            UpdateLabelStatus();
        }

        private void Global_CurrentlySelectedFolderChanged(string folder)
        {
            textBoxFolderNew.Text = Global.CurrentlySelectedFolder;
        }

        private void UpdateLabelStatus()
        {
            if(currentlySelectedFolderToEmail == null)
            {
                labelStatus.Text = "<== Select entry to edit or add a new entry below";
            }
            else
            {
                labelStatus.Text = "Edit or delete entry or add a new entry below";
            }
        }

        private void UpdateList()
        {
            var currentlySelectedEntry = listBoxFolderToEmail.SelectedIndex;
            listBoxFolderToEmail.DataSource = folderToEmailList.Data;
            listBoxFolderToEmail.SelectedIndex = currentlySelectedEntry;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            
        }

        private void FolderToEmailListForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Global.CurrentlySelectedFolderChanged -= Global_CurrentlySelectedFolderChanged;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            folderToEmailList.TryAdd(new FolderToEMail(false, textBoxFolderNew.Text, ""));

            listBoxFolderToEmail_SelectedIndexChanged(sender, null);

            Global.folderToEMailList.Save();
        }

        private void listBoxFolderToEmail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxFolderToEmail.SelectedItem == null) return;

            currentlySelectedFolderToEmail = (FolderToEMail)listBoxFolderToEmail.SelectedItem;

            textBoxEmail.Text = currentlySelectedFolderToEmail.Email;
            textBoxFolder.Text = currentlySelectedFolderToEmail.Folder;
            checkBoxEnabled.Checked = currentlySelectedFolderToEmail.Enabled;
            labelId.Text = currentlySelectedFolderToEmail.Id.ToString();

            UpdateLabelStatus();

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            currentlySelectedFolderToEmail.Email = textBoxEmail.Text.Trim();
            currentlySelectedFolderToEmail.Folder = textBoxFolder.Text.Trim();
            textBoxEmail.Text = currentlySelectedFolderToEmail.Email;
            textBoxFolder.Text = currentlySelectedFolderToEmail.Folder;
            currentlySelectedFolderToEmail.Enabled = checkBoxEnabled.Checked;
            textBoxFolder.ReadOnly = true;
            buttonSave.Enabled = false;
            currentlySelectedFolderToEmail.TriggerChanged();

            Global.folderToEMailList.Save();
            UpdateLabelStatus();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            folderToEmailList.Data.Remove(currentlySelectedFolderToEmail);
            textBoxEmail.Text = "";
            textBoxFolder.Text = "";
            checkBoxEnabled.Checked = false;
            labelId.Text = "";
            currentlySelectedFolderToEmail = null;

            Global.folderToEMailList.Save();
            UpdateLabelStatus();
        }

        private void textBoxEmail_TextChanged(object sender, EventArgs e)
        {
            if(currentlySelectedFolderToEmail != null) buttonSave.Enabled = true;
        }

        private void checkBoxEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (currentlySelectedFolderToEmail != null) buttonSave.Enabled = true;
        }

        private void textBoxFolder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            textBoxFolder.ReadOnly = false;
        }

        private void labelStatus_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
