﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhipsisOutlookTools.Forms.DebugForms
{
    public partial class DebugForm : Form
    {
        public DebugForm()
        {
            InitializeComponent();

            labelDebuggerAttached.Text = Debugger.IsAttached ? "Yes" : "No";
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            Logger.LogWrite += Logger_LogWrite;
        }

        private void Logger_LogWrite(string line)
        {
            if(this.InvokeRequired)
            {
                this.Invoke((MethodInvoker) delegate ()
                {
                    Logger_LogWrite(line);
                });
                return;
            }
            textBoxLogger.AppendText(line + Environment.NewLine);
        }

        private void DebugForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Logger.LogWrite -= Logger_LogWrite;
        }
    }
}
