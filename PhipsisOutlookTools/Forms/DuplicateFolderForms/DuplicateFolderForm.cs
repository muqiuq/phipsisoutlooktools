﻿using PhipsisOutlookTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhipsisOutlookTools.Forms.DuplicateFolderForms
{
    public partial class DuplicateFolderForm : Form
    {
        DuplicateFolderEntry currentlySelectedEntry;

        public DuplicateFolderForm()
        {
            InitializeComponent();

            listBoxDuplicateFolders.DataSource = Global.duplicateFolderList.Data;

            UpdateCurrentlySelectedItem();
        }

        private void UpdateCurrentlySelectedItem()
        {
            if(currentlySelectedEntry != null)
            {
                textBoxEmail.Text = currentlySelectedEntry.Name;
                textBoxFolderKeep.Text = currentlySelectedEntry.FolderPathToKeepItem;
                textBoxFolderDelete.Text = currentlySelectedEntry.FolderPathToDeleteItem;
                labelId.Text = currentlySelectedEntry.Id.ToString();
                checkBoxEnabled.Checked = currentlySelectedEntry.Enabled;


                textBoxEmail.Enabled = true;
                textBoxFolderDelete.Enabled = true;
                textBoxFolderKeep.Enabled = true;
                buttonSave.Enabled = false;
                buttonDelete.Enabled = true;
            }
            else
            {
                textBoxEmail.Text = "";
                textBoxFolderKeep.Text = "";
                textBoxFolderDelete.Text = "";
                labelId.Text = "-";
                checkBoxEnabled.Checked = false;
                textBoxEmail.Enabled = false;
                textBoxFolderDelete.Enabled = false;
                textBoxFolderKeep.Enabled = false;
                buttonSave.Enabled = false;
                buttonDelete.Enabled = false;
            }
        }

        private void DuplicateFolderForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var dfe = Global.duplicateFolderList.Add(
                "new","",""
                );
        }

        private void listBoxDuplicateFolders_SelectedValueChanged(object sender, EventArgs e)
        {
            currentlySelectedEntry = (DuplicateFolderEntry)listBoxDuplicateFolders.SelectedItem;

            UpdateCurrentlySelectedItem();

            Global.duplicateFolderList.Save();
        }

        private void editFieldsTextChanged(object sender, EventArgs e)
        {
            buttonSave.Enabled = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (currentlySelectedEntry == null) return;

            currentlySelectedEntry.Name = textBoxEmail.Text.Trim();
            currentlySelectedEntry.FolderPathToKeepItem = textBoxFolderKeep.Text.Trim();
            currentlySelectedEntry.FolderPathToDeleteItem = textBoxFolderDelete.Text.Trim();
            currentlySelectedEntry.Enabled = checkBoxEnabled.Checked;

            currentlySelectedEntry.TriggerChanged();

            UpdateCurrentlySelectedItem();

            Global.duplicateFolderList.Save();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Global.duplicateFolderList.Data.Remove(currentlySelectedEntry);

            currentlySelectedEntry = null;

            UpdateCurrentlySelectedItem();

            Global.duplicateFolderList.Save();
        }

        private void editFieldFolderTextChanged(object sender, EventArgs e)
        {
            var tb = (TextBox) sender;

            if(tb.Text.Trim() == ".")
            {
                tb.Text = Global.CurrentlySelectedFolder;
            }

            editFieldsTextChanged(sender, e);
        }

        private void checkBoxEnabled_CheckedChanged(object sender, EventArgs e)
        {
            editFieldsTextChanged(sender, e);
        }
    }
}
