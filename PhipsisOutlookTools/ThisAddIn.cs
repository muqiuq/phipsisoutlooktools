﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Core;
using Microsoft.Office.Tools.Ribbon;
using System.Diagnostics;
using System.IO;
using System.Threading;

// https://docs.microsoft.com/en-us/office/client-developer/outlook/pia/events-in-the-outlook-pia

namespace PhipsisOutlookTools
{
    public partial class ThisAddIn
    {
        private Outlook.Explorer currentExplorer;
        private Outlook.Inspectors currentInspectors;
        Outlook.MailItem newInspectorMailItem;

        Logger logger = new Logger("ThisAddIn");

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            currentExplorer = this.Application.ActiveExplorer();
            currentInspectors = this.Application.Inspectors;
            currentExplorer.SelectionChange += CurrentExplorer_SelectionChange;
            currentInspectors.NewInspector += CurrentInspectors_NewInspector;
            this.Application.ItemSend += Application_ItemSend;

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            Global.Version = fvi.FileVersion;

            if (!Directory.Exists(Global.UserProfilePath))
            {
                Directory.CreateDirectory(Global.UserProfilePath);
            }

            Global.folderToEMailList = new Models.FolderToEMailList(
                Path.Combine(Global.UserProfilePath, "foldertoemail.json"));

            Global.duplicateFolderList = new Models.DuplicateFolderList(
                Path.Combine(Global.UserProfilePath, "duplicatefolders.json"));

            Global.folderToEMailList.TryLoad();
            Global.duplicateFolderList.TryLoad();

            logger.Info("Plugin loaded");

            CurrentExplorer_SelectionChange();
        }

        private void Application_ItemSend(object Item, ref bool Cancel)
        {
            if(Item is Outlook.MailItem)
            {
                var currentMailItem = (Outlook.MailItem)Item;

                if (currentMailItem == null) return;

                if (currentMailItem.SaveSentMessageFolder == null) return;

                var folder = (Outlook.MAPIFolder) newInspectorMailItem.SaveSentMessageFolder;

                Debug.WriteLine(folder.FullFolderPath);

                var dfes = Global.duplicateFolderList.SearchByFolderFullPath(folder.FullFolderPath);

                if(dfes != null && dfes.Length > 0)
                {
                    logger.Info("ItemSend event triggered and matched to {0} activating {1} entry/ies", folder.FullFolderPath, dfes.Length.ToString());

                    var dict = new DuplicateItemCheckerThread(dfes, currentMailItem, this);

                    dict.Start();
                }

            }
        }

        Outlook.Inspector currentInspector;

        private void CurrentInspectors_NewInspector(Outlook.Inspector Inspector)
        {
            if(Inspector.CurrentItem is Outlook.MailItem)
            {
                newInspectorMailItem = Inspector.CurrentItem;

                if (newInspectorMailItem.EntryID == null && !newInspectorMailItem.Sent)
                {
                    logger.Info("Attachting event to New-MailItem-Inspector");
                    ((Outlook.InspectorEvents_Event)Inspector).Activate += ThisInspector_Activate;
                    currentInspector = Inspector;
                }
            }
        }

        private void ThisInspector_Activate()
        {
            if (newInspectorMailItem.EntryID == null && !newInspectorMailItem.Sent)
            {
                ((Outlook.InspectorEvents_Event)currentInspector).Activate -= ThisInspector_Activate;
                var folderToEMail = Global.folderToEMailList.SearchByFolder(Global.CurrentlySelectedFolder);
                logger.Info("{0} {1}",
                    folderToEMail == null ? "Not found" : "Found",
                    Global.CurrentlySelectedFolder);

                if (folderToEMail != null)
                {

                    newInspectorMailItem.SentOnBehalfOfName = folderToEMail.Email;
                    newInspectorMailItem.Recipients.Add(folderToEMail.Email);
                    newInspectorMailItem.Recipients.ResolveAll();
                    var addressEntry = newInspectorMailItem.Recipients[newInspectorMailItem.Recipients.Count].AddressEntry;
                    newInspectorMailItem.Recipients.Remove(newInspectorMailItem.Recipients.Count);
                    newInspectorMailItem.Sender = addressEntry;

                    logger.Info("Setting Sender to ", folderToEMail.Email);

                    Debug.WriteLine(string.Format("SentOnBehalfOfName: {0}", newInspectorMailItem.SentOnBehalfOfName ?? "NULL"));
                    Debug.WriteLine(string.Format("ReceivedByName: {0}", newInspectorMailItem.ReceivedByName ?? "NULL"));
                }
            }
        }

        private void CurrentExplorer_SelectionChange()
        {
            Outlook.MAPIFolder selectedFolder =
        this.Application.ActiveExplorer().CurrentFolder;
            String expMessage = string.Format("Your current folder is {0},{1},{2}",
                selectedFolder.Name,
                selectedFolder.FolderPath,
                selectedFolder.FullFolderPath
                );

            Global.ChangeCurrentlySelectedFolder(selectedFolder.FullFolderPath);

            logger.Info(expMessage);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
            currentExplorer.SelectionChange -= CurrentExplorer_SelectionChange;
            currentInspectors.NewInspector -= CurrentInspectors_NewInspector;
        }


        protected override IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return Globals.Factory.GetRibbonFactory().CreateRibbonManager(new IRibbonExtension[] { new BasicToolsRibbon() });
        }

        public Outlook.MAPIFolder GetFolder(string fullpath)
        {
            Outlook.MAPIFolder currentFolder;
            List<Outlook.MAPIFolder> folders = new List<Outlook.MAPIFolder>();

            if(fullpath.StartsWith("\\"))
            {
                fullpath = fullpath.Remove(0, 2);
            }

            var pathElements = fullpath.Split('\\');


            currentFolder = this.Application.Session.Folders[pathElements[0]];
            for(int a = 1; a < pathElements.Length; a++)
            {
                currentFolder = currentFolder.Folders[pathElements[a]];
            }

            return currentFolder;
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
