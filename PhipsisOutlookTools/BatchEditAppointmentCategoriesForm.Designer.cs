﻿namespace PhipsisOutlookTools
{
    partial class BatchEditAppointmentCategoriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.categoryTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numOfMatchLabel = new System.Windows.Forms.Label();
            this.countButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.selectedFolderLabel = new System.Windows.Forms.Label();
            this.setFolderButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.removeButton = new System.Windows.Forms.Button();
            this.singleCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Categorie name";
            // 
            // categoryTextBox
            // 
            this.categoryTextBox.Location = new System.Drawing.Point(124, 69);
            this.categoryTextBox.Name = "categoryTextBox";
            this.categoryTextBox.Size = new System.Drawing.Size(203, 20);
            this.categoryTextBox.TabIndex = 1;
            this.categoryTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Num of matches";
            // 
            // numOfMatchLabel
            // 
            this.numOfMatchLabel.AutoSize = true;
            this.numOfMatchLabel.Location = new System.Drawing.Point(124, 130);
            this.numOfMatchLabel.Name = "numOfMatchLabel";
            this.numOfMatchLabel.Size = new System.Drawing.Size(27, 13);
            this.numOfMatchLabel.TabIndex = 3;
            this.numOfMatchLabel.Text = "N/A";
            // 
            // countButton
            // 
            this.countButton.Location = new System.Drawing.Point(124, 96);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(75, 23);
            this.countButton.TabIndex = 4;
            this.countButton.Text = "Count";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Location = new System.Drawing.Point(12, 302);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(21, 20);
            this.statusLabel.TabIndex = 5;
            this.statusLabel.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Folder:";
            // 
            // selectedFolderLabel
            // 
            this.selectedFolderLabel.AutoSize = true;
            this.selectedFolderLabel.Location = new System.Drawing.Point(124, 13);
            this.selectedFolderLabel.Name = "selectedFolderLabel";
            this.selectedFolderLabel.Size = new System.Drawing.Size(69, 13);
            this.selectedFolderLabel.TabIndex = 7;
            this.selectedFolderLabel.Text = "Not Selected";
            // 
            // setFolderButton
            // 
            this.setFolderButton.Location = new System.Drawing.Point(124, 36);
            this.setFolderButton.Name = "setFolderButton";
            this.setFolderButton.Size = new System.Drawing.Size(137, 23);
            this.setFolderButton.TabIndex = 8;
            this.setFolderButton.Text = "set current folder";
            this.setFolderButton.UseVisualStyleBackColor = true;
            this.setFolderButton.Click += new System.EventHandler(this.setFolderButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.Location = new System.Drawing.Point(16, 267);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(311, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 9;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(124, 153);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 10;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // singleCheckBox
            // 
            this.singleCheckBox.AutoSize = true;
            this.singleCheckBox.Checked = true;
            this.singleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.singleCheckBox.Location = new System.Drawing.Point(206, 157);
            this.singleCheckBox.Name = "singleCheckBox";
            this.singleCheckBox.Size = new System.Drawing.Size(55, 17);
            this.singleCheckBox.TabIndex = 11;
            this.singleCheckBox.Text = "Single";
            this.singleCheckBox.UseVisualStyleBackColor = true;
            // 
            // BatchEditAppointmentCategoriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 331);
            this.Controls.Add(this.singleCheckBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.setFolderButton);
            this.Controls.Add(this.selectedFolderLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.numOfMatchLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.categoryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "BatchEditAppointmentCategoriesForm";
            this.Text = "BatchEditAppointmentCategoriesForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox categoryTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label numOfMatchLabel;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label selectedFolderLabel;
        private System.Windows.Forms.Button setFolderButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.CheckBox singleCheckBox;
    }
}