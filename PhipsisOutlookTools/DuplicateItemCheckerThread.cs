﻿using Microsoft.Office.Interop.Outlook;
using PhipsisOutlookTools.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhipsisOutlookTools
{
    public class DuplicateItemCheckerThread
    {

        private MinimalMailItem minimalMailItem;
        private MailItem mailitem;
        private bool foundInSource = false;
        private bool foundInDestination = false;
        private MailItem mailItemToDelete;
        private Thread t;
        private DuplicateFolderEntry[] dfes;
        private ThisAddIn thisAddIn;
        private List<MAPIFolder> toKeepInFolders = new List<MAPIFolder>();
        private List<MAPIFolder> toDeleteInFolders = new List<MAPIFolder>();
        private Logger logger = new Logger("DuplicateItemCheckerThread");


        public DuplicateItemCheckerThread(
            DuplicateFolderEntry[] dfes, 
            Microsoft.Office.Interop.Outlook.MailItem mailitem,
            ThisAddIn thisAddIn)
        {
            this.dfes = dfes;
            this.minimalMailItem = new MinimalMailItem( mailitem);
            this.mailitem = mailitem;
            this.thisAddIn = thisAddIn;

            foreach(var dfe in dfes)
            {
                var toKeepInFolder = thisAddIn.GetFolder(dfe.FolderPathToKeepItem);
                var toDeleteInFolder = thisAddIn.GetFolder(dfe.FolderPathToDeleteItem);

                toKeepInFolder.Items.ItemAdd += Items_ItemAdd_ToKeepInFolder;
                toDeleteInFolder.Items.ItemAdd += Items_ItemAdd_ToDeleteInFolder;

                toKeepInFolders.Add(toKeepInFolder);
                toDeleteInFolders.Add(toDeleteInFolder);
            }

            logger.Info("Started DuplicateItemCheckerThread for ({0}) {1}", dfes[0].Id.ToString(),
                dfes.Select(p => p.Name).Aggregate("", (c, n) => c + ", " + n));
        }


        private void Items_ItemAdd_ToDeleteInFolder(object Item)
        {
            if(Item is MailItem)
            {
                var addedmailItem = (Microsoft.Office.Interop.Outlook.MailItem)Item;
                var addedMailItemMinimal = new MinimalMailItem(addedmailItem);
                if(addedMailItemMinimal.Equals(minimalMailItem))
                {
                    foundInDestination = true;
                    mailItemToDelete = addedmailItem;
                    logger.Info("Found in folder to delete!");
                }
                else
                {
                    logger.Info("(DELETE) Did not match: {0}", minimalMailItem.ToString());
                }
            }
        }

        private void Items_ItemAdd_ToKeepInFolder(object Item)
        {
            if (Item is MailItem)
            {
                var addedMailItem = new MinimalMailItem((Microsoft.Office.Interop.Outlook.MailItem)Item );
                if (addedMailItem.Equals(minimalMailItem))
                {
                    foundInSource = true;
                    logger.Info("Found in folder to keep!");
                }
                else
                {
                    logger.Info("(KEEP) Did not match: {0}", minimalMailItem.ToString());
                }
            }
        }



        public void Start()
        {
            if (t != null && t.IsAlive) return;
            t = new Thread(run);
            t.Start();
        }

        public void Stop()
        {
            t.Interrupt();
        }

        private void run()
        {
            try
            {
                int waitTimeInSecs = 100;
                for (int a = 0; a < waitTimeInSecs; a++)
                {
                    if(a % 10 == 0)
                    {
                        logger.Info("{0} seconds remaining for the message to appear", (waitTimeInSecs - a).ToString());
                    }
                    if(foundInDestination && foundInSource)
                    {
                        var entryId = mailItemToDelete.EntryID;
                        var creationTime = mailItemToDelete.ReceivedTime;
                        var diffNowVsCreationTime = DateTime.Now - creationTime;

                        logger.Info("Found mailitem in both locations");

                        if(diffNowVsCreationTime.TotalMinutes <= 3)
                        {
                            try
                            {
                                mailItemToDelete.Delete();
                                logger.Info(string.Format("Deleted MailItem ({0}, {1})",
                                entryId,
                                creationTime.ToString()
                                ));
                                break;
                            }catch(COMException comEx)
                            {
                                logger.Error("Could not delete because of COMException. Maybe synchronization delay. Retrying...");
                                logger.Error(comEx.ToString());
                            }
                        }
                        else
                        {
                            logger.Error("Error: E-Mail Creation Date is bigger then 5 minutes. Ignoring");
                            break;
                        }
                        
                    }

                    Thread.Sleep(1000);
                }
                if (!foundInDestination || !foundInSource)
                {
                    logger.Error("Failed to find and delete duplicate of MailItem");
                }
            }
            catch(System.Exception ex)
            {
                logger.Error(ex.ToString());
            }
            foreach(var toKeepInFolder in toKeepInFolders)
            {
                toKeepInFolder.Items.ItemAdd -= Items_ItemAdd_ToKeepInFolder;
            }
            foreach (var toDeleteInFolder in toDeleteInFolders)
            {
                toDeleteInFolder.Items.ItemAdd -= Items_ItemAdd_ToDeleteInFolder;
            }

        }

    }
}
