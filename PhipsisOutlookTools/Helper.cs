﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhipsisOutlookTools
{
    public class Helper
    {

        public static string RecipientsToString(Microsoft.Office.Interop.Outlook.Recipients recipients)
        {
            const string PR_SMTP_ADDRESS =
                "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
            Microsoft.Office.Interop.Outlook.Recipients recips = recipients;
            var recipientsEmails = new List<string>();
            foreach (Microsoft.Office.Interop.Outlook.Recipient recip in recips)
            {
                Microsoft.Office.Interop.Outlook.PropertyAccessor pa = recip.PropertyAccessor;
                string smtpAddress =
                    pa.GetProperty(PR_SMTP_ADDRESS).ToString();
                recipientsEmails.Add(smtpAddress);
            }
            return string.Join("; ", recipientsEmails);
        }

    }
}
