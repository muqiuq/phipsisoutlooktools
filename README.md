# Phipsis Outlook Tools

![Ribbon](images/screenshot_ribbon.PNG)

This Outlook plugin is a collection of features I needed for different clients. It is only tested locally (Outlook 2019) and comes without any warranty. 
 - **Edit calendar categories** in a text editor (also useful to export and import)
 - **Map sender (from) email address to folder (Configurable)**:<br>Allows to automatically set the sender depending on the currently selected folder. Simply configure an override for the desired folder in the configuration form. A wildcard may be used to apply the configuration to multiple folders. 
 - **Prevent duplication of emails (Configurable)**:<br>Whenever you send an email the plugin checks predefined folders for duplicates and deletes one of them. Simply configure the folder to "keep the item" and the folder to "delete the item" in the configuration form. 
 - All configuration is stored in single *json* files in 
```
%APPDATA%\PhipsisOutlookTools
```

**Looking for better naming of the buttons. Send ideas to contact below :-)**

# Keywords and related errors
 - MessageCopyForSentAsEnabled / MessageCopyForSendOnBehalfEnabled:
     + https://docs.microsoft.com/en-us/exchange/troubleshoot/user-and-shared-mailboxes/sent-mail-is-not-saved
 - https://www.michev.info/Blog/Post/1141/keep-a-copy-of-sent-emails-for-delegates-of-user-mailboxes
 - https://community.spiceworks.com/topic/2082972-exchange-online-shared-mailbox-send-as-sent-items


# Contributions
<div>Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

# Contact
info@portup.ch
